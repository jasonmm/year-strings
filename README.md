# Year Strings

A PHP library for parsing strings of years and year ranges.

# Usage

```php
$years = \ShadyBrookSoftware\YearStrings\parseYearsString("1980,1982-1984,1990");

/* $years will be => [1980, 1982, 1983, 1984, 1990] */
```
