<?php

declare(strict_types=1);


class FunctionsTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider sortDataProvider
     *
     * @param array $expected
     * @param array $data
     */
    public function testSort(array $expected, array $data)
    {
        $this->assertEquals(
            $expected,
            \ShadyBrookSoftware\YearStrings\sort(
                $data,
                function ($a, $b) {
                    return $a <=> $b;
                }));
    }

    public function sortDataProvider()
    {
        return [
            [[1, 2, 3], [3, 1, 2]],
        ];
    }

    /**
     * @param array  $expected
     * @param string $yearStr
     *
     * @dataProvider expandYearsProvider
     */
    public function testExpandYears(array $expected, string $yearStr)
    {
        $this->assertEquals($expected, \ShadyBrookSoftware\YearStrings\expandYears($yearStr));
    }

    public function expandYearsProvider()
    {
        return [
            [[1980], "1980"],
            [[1980, 1981, 1982, 1983], "1980-1983"],
            [[1980, 1981, 1982, 1983], "1980 - 1983"],
        ];
    }

    /**
     * @param array  $expected
     * @param string $yearStr
     * @dataProvider parseYearsStringProvider
     */
    public function testParseYearsString(array $expected, string $yearStr)
    {
        $this->assertEquals($expected, \ShadyBrookSoftware\YearStrings\parseYearsString($yearStr));
    }

    public function parseYearsStringProvider()
    {
        return [
            [[1980], "1980"],
            [[1980, 1981, 1982, 1983], "1980-1983"],
            [[1980, 1981, 1982, 1983], "1980 - 1983"],
            [[1980, 1982, 1983, 1984, 1990], "1980,1982-1984,1990"],
            [[1980, 1982, 1983, 1984, 1990], "1980, 1982-1984, 1990"],
        ];
    }
}