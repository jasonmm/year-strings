<?php

declare(strict_types=1);

namespace ShadyBrookSoftware\YearStrings;

/**
 * Calls `usort` on a copy of $a and returns the sorted copy.
 *
 * @param array    $a
 * @param callable $compareFn
 *
 * @return array
 */
function sort(array $a, callable $compareFn): array
{
    $aCopy = $a;

    usort($aCopy, $compareFn);

    return $aCopy;
}

/**
 * Takes a string containing either a single year (e.g. "1980") or a string
 * containing a range of years (e.g. "1980-1983") and returns an array of the
 * years ([1980] for the single year, or [1980, 1981, 1982, 1983] for a range).
 *
 * All years are converted to integers, so any non-integer strings will be
 * converted a zero.
 *
 * @param string $yearStr
 *
 * @return array
 */
function expandYears(string $yearStr): array
{
    if (strpos($yearStr, '-') !== false) {
        $r = explode('-', $yearStr);

        return range(intval($r[0]), intval($r[1]));
    }

    return [intval($yearStr)];
}

/**
 * Takes a CSV string of years and year ranges returns an array of individual
 * years.
 *
 * @param string $yearsStr e.g. "1980", or "1980-1983", or "1980,1985-1987,1990"
 *
 * @return array
 */
function parseYearsString(string $yearsStr): array
{
    return
        sort(
            array_filter(
                array_unique(
                    array_flatten(
                        array_map(
                            'ShadyBrookSoftware\YearStrings\expandYears',
                            explode(',', $yearsStr)))),
                function ($item) {
                    return $item !== 0;
                }),
            function (int $a, int $b) {
                return $a <=> $b;
            });
}
